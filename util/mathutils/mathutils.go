package mathutils

func MaxInt(x, y int) int {
	if (x >= y) {
		return x
	} else {
		return y
	}
}
func MaxInt64(x, y int64) int64 {
	if (x >= y) {
		return x
	} else {
		return y
	}
}