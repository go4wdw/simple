package randomutils

import (
	"math/rand"
	"fmt"
	"time"

	"wdw/simple/util/stringutils"
)



func init() {
	rand.Seed(time.Now().UnixNano())
}

// GetInt(): non-negative int
// GetInt(max): [0, max)
// GetInt(min, max): [min, max)
// others: panic
func GetInt(ints ...int) int {
	len := len(ints)
	switch len {
	case 0:
		return rand.Int()
	case 1:
		return rand.Intn(ints[0])
	case 2:
		min, max := ints[0], ints[1]
		r := rand.Intn(max - min)
		return r + min
	}

	panic(fmt.Sprintf("illegal argument number: %v", ints))
}

var STR string = "wdw"

func GetStr(str string, length int) string {
	if len(str) == 0 {
		str = stringutils.LETTER_DIGIT
	}
	var originLen = len(str)
	var rslt string
	for i :=  0; i < length; i++ {
		idx := GetInt(originLen)
		rslt += str[idx:idx + 1]
	}
	return rslt

}