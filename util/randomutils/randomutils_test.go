package randomutils

import (
	"testing"
	"log"
	"math/rand"
	"time"
)

func init() {
	log.Flags()
}


func TestRand(t *testing.T) {
	log.Println(rand.Int())
	for i := 0; i < 100; i++ {
		log.Println(GetInt(-10, 10))
	}

}

func TestRand2(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	log.Println(rand.Int())
}

func TestInt(t *testing.T) {
	log.Println(GetInt())
	log.Println(GetInt(100))
	log.Println(GetInt(100, 200))
}


func TestGetStr(t *testing.T) {
	log.Println(GetStr("", 8))
}