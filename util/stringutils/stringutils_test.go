package stringutils

import (
	"fmt"
	"log"
	"runtime"
	"testing"
)

func init() {
	log.Flags()
	fmt.Print("")
}

func Wdw() {

}

func TestGetSrcFile(t *testing.T) {
	fmt.Println(GetSrcFile())
	fmt.Println(GetSrcDir())
}


func TestStruct2String(t *testing.T) {
	var memstat runtime.MemStats
	runtime.ReadMemStats(&memstat)
	fmt.Printf("memstat|%s\n", Struct2String(memstat, "PauseNs", "BySize"))
}

func TestStruct2String2(t *testing.T) {
	type Demo struct {
		Name string
		x, Y int
		u float32
		_ float32  // padding
		A *[]int
		F func()
	}
	var demo Demo
	demo.x = 1
	demo.Y = 2
	demo.Name = "hi"
	fmt.Printf("demo|%s\n", Struct2String(&demo))
}

func TestGetCaller(t *testing.T) {
	file, funcName, line := GetCaller(1)
	fmt.Printf("file=%s, func=%s, line=%d\n", file, funcName, line)
}

func TestLineSeparator(t *testing.T) {
	log.Println(LineSeparator())
}
