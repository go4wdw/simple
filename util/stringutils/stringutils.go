// string util
package stringutils

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	//"bytes"
	//"io"
	//"io/ioutil"
	//"fmt"
	//"os"
	"crypto/rand"
)

var lineSeparator string = ""

func init() {
	switch os := runtime.GOOS; os {
	case "windows":
		lineSeparator = "\r\n"
	case "darwin":
		lineSeparator = "\n"
	default:
		lineSeparator = "\n"
	}
}

const (
	DIGIT        = "0123456789"
	LETTER_LOWER = "abcdefghijklmnopqrstuvwxyz"
	LETTER_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	LETTER       = LETTER_LOWER + LETTER_UPPER
	LETTER_DIGIT = LETTER + DIGIT
)

func LineSeparator() string {
	return lineSeparator
}

func GetSrcFile() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		return ""
	}
	return file

}
func GetSrcDir() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		return ""
	}
	return filepath.Dir(file)

}
func GetCaller(calldepth int) (file string, funcName string, line int) {

	pc_, file_, line_, ok_ := runtime.Caller(calldepth)
	if !ok_ {
		file = "?"
		funcName = "?"
		line = 0
	} else {
		file = filepath.Base(file_)
		file = strings.TrimSuffix(file, filepath.Ext(file))
		
		line = line_

		func_ := runtime.FuncForPC(pc_)
		if nil != func_ {
			funcName = func_.Name()
			idx := strings.LastIndex(funcName, ".")
			if (idx >= 0) {
				funcName = funcName[idx+1:]
			}
		}
	}

	return
}

func S2I(str string, def int) int {
	i, err := strconv.Atoi(str)
	if nil == err {
		return i
	} else {
		return def
	}
}
func I2S(val int) string {
	return strconv.Itoa(val)
}

func InString(str string, dests ...string) bool {
	for _, v := range dests {
		if v == str {
			return true
		}
	}
	return false
}

func Struct2Slice(a interface{}, ignores ...string) []string {
	val := reflect.ValueOf(a)
	kind := val.Kind()

	if kind  == reflect.Ptr { // get value when it is a pointer
		val = val.Elem()
		kind = val.Kind()
	}
	if kind != reflect.Struct {
		panic(fmt.Errorf("not struct: %s", kind.String()))
	}
	rslts := make([]string, 0, val.NumField())

	for i := 0; i < val.NumField(); i++ {
		v := val.Field(i)
		t := val.Type().Field(i)
		if !t.IsExported() {
			continue
		}
		if InString(t.Name, ignores...) {
			continue
		}
		rslts = append(rslts, fmt.Sprintf("%s: %v", t.Name, v.Interface()))
	}
	return rslts
}
func Struct2String(a interface{}, ignores ...string) string {
	return strings.Join(Struct2Slice(a, ignores...), ", ")
}
func Pb2Slice(a interface{}, ignores ...string) []string {
	return Struct2Slice(a, append(ignores, "XXX_NoUnkeyedLiteral", "XXX_unrecognized", "XXX_sizecache")...)
}
func Pb2String(a interface{}, ignores ...string) string {
	return strings.Join(Pb2Slice(a, ignores...), ", ")
}

func GroupNumber(n int64) string {
	in := strconv.FormatInt(n, 10)
	out := make([]byte, len(in)+(len(in)-2+int(in[0]/'0'))/3)
	if in[0] == '-' {
		in, out[0] = in[1:], '-'
	}

	for i, j, k := len(in)-1, len(out)-1, 0; ; i, j = i-1, j-1 {
		out[j] = in[i]
		if i == 0 {
			return string(out)
		}
		if k++; k == 3 {
			j, k = j-1, 0
			out[j] = ','
		}
	}
}




func UUID() (uuid string) {

    b := make([]byte, 16)
    _, err := rand.Read(b)
    if err != nil {
        fmt.Println("uuid error: ", err)
        return
    }

    uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
    return
}
