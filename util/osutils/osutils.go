package osutils

import (
	"runtime"
	"os"
	"os/signal"
	"syscall"

	"wdw/simple/util/stringutils"
)

func IsWin() bool {
	return runtime.GOOS == "windows"
}

func MemStat() string {
	var memstat runtime.MemStats
	runtime.ReadMemStats(&memstat)
	return stringutils.Struct2String(memstat, "PauseNs", "BySize")
}

// signal.Ignore(syscall.SIGPIPE) not working
func IgnoreSIGPIPE() {
	sc := make(chan os.Signal, 1)
	signal.Notify(sc,
	syscall.SIGPIPE)

	go func() {
		for {
			select {
			case sig := <-sc:
				// do not write, prevent dead loop
				//fmt.Fprintf(os.Stderr, "%v\n", sig) // 
				if sig == syscall.SIGPIPE {
				}
			}
		}
	}()
}