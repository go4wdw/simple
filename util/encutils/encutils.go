package encutils

import (
	"bytes"
	"io/ioutil"

	"golang.org/x/text/encoding/simplifiedchinese"
	//"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
)


func GBK2UTF8(bs []byte) ([]byte, error) {
	I := bytes.NewReader(bs)
	O := transform.NewReader(I, simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(O)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func UTF82GBK(bs []byte) ([]byte, error) {
    reader := transform.NewReader(bytes.NewReader(bs), simplifiedchinese.GBK.NewEncoder())
    d, e := ioutil.ReadAll(reader)
    if e != nil {
        return nil, e
    }
    return d, nil
}

