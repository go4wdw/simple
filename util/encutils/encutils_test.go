package encutils

import (
	"fmt"
	"log"
	"testing"
)

func init() {
	log.Flags()
	fmt.Print("")
}

func Wdw() {

}


// 中国 D6D0B9FA E4B8ADE59BBD

func TestGBK2UTF8(t *testing.T) {
	bs := [...]byte{'\xD6', '\xD0', '\xB9', '\xFA'}
	newBs, _ := GBK2UTF8(bs[:])
	fmt.Printf("%X, %X", bs, newBs)
}


func TestUTF82GBK(t *testing.T) {
	bs := [...]byte{'\xE4', '\xB8', '\xAD'}
	newBs, _ := UTF82GBK(bs[:])
	fmt.Printf("%X, %X", bs, newBs)
}

