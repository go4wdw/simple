package errorutils

import (
	"runtime"
	"fmt"
	"path/filepath"
)

func Errorf(format string, a ...interface{}) error {
	_, file, line, _ := runtime.Caller(1)
	msg := fmt.Sprintf(format, a...)
	return fmt.Errorf("%s:%d|ERROR|%s", filepath.Base(file), line, msg)
}
func Error(a ...interface{}) error {
	_, file, line, _ := runtime.Caller(1)
	msg := fmt.Sprint(a...)
	return fmt.Errorf("%s:%d|ERROR|%s", filepath.Base(file), line, msg)
}
