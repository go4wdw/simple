package reflectutils

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"testing"
)

type Inter interface {
	m() int
}

type Impl struct {
	i int
	str string
}
func (i Impl) m() int {
	return 3;
}



func init() {
	log.Flags()
	log.SetFlags(log.Lshortfile)
	fmt.Print("")
}

func TestWhatIsIt(t *testing.T) {
	log.Println(WhatIsIt(3))
	log.Println(WhatIsIt(3.5))

	var p *int
	var i = 5;
	p = &i;

	log.Println(WhatIsIt(reflect.ValueOf(i).Interface()))
	log.Println(WhatIsIt(p))
	log.Println(WhatIsIt(&p))
	log.Println(WhatIsIt(i))

	primes := [6]int{2, 3, 5, 7, 11, 13} // array
	log.Println(WhatIsIt(primes))
	log.Println(WhatIsIt(&primes))
	log.Println(WhatIsIt(primes[0:]))

	m := make(map[string]interface{})
	m["one"] = 1
	m["two"] = 2
	m["three"] = "333"
	log.Println(WhatIsIt(m))
	log.Println(WhatIsIt(m["one"]))
	log.Println(WhatIsIt(m["three"]))


	var inter Inter
	log.Println(WhatIsIt(inter))
	impl := Impl{5, "wdw"}
	log.Printf("%v\n", impl)
	inter = impl
	log.Printf("%v\n", inter)
	log.Println(WhatIsIt(inter))
	inter = &impl
	log.Printf("%v\n", inter)
	log.Println(WhatIsIt(inter))

	var any interface{}
	log.Println(WhatIsIt(any))
	any = 3
	log.Println(WhatIsIt(any))
	any = "wdw"
	log.Println(WhatIsIt(any))
	
	inter = impl
	any = inter
	log.Println(WhatIsIt(any))

	var r io.Reader
	log.Println(WhatIsIt(r))
	r = os.Stdin
	log.Println(WhatIsIt(r))
	r = bufio.NewReader(r)
	log.Println(WhatIsIt(r))
	r = new(bytes.Buffer)
	log.Println(WhatIsIt(r))

	
	log.Println(WhatIsIt(func() {}))

}