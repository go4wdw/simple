package reflectutils

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

/*
switch r := x.(type) {
case bool:
	return fmt.Sprintf("bool: %v", r)
case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
	return fmt.Sprintf("integer: %v", r)
case float32, float64, complex64, complex128:
	return fmt.Sprintf("floating-point: %v", r)
case string:
	return fmt.Sprintf("string: %v", r)
default:
}
*/


func WhatIsIt(x interface{}) string {
	if nil == x {
		return "nil"
	}

	typ := reflect.TypeOf(x)
	value := reflect.ValueOf(x)

	switch typ.Kind() {
	case reflect.Bool:
		return "bool: " + strconv.FormatBool(value.Bool())
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return value.Kind().String() + ": " + strconv.FormatInt(value.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return value.Kind().String() + ": " + strconv.FormatUint(value.Uint(), 10)
	case reflect.Float32, reflect.Float64:
		return value.Kind().String() + ": " + fmt.Sprintf("%g", value.Float())
	case reflect.Complex64, reflect.Complex128:
		return value.Kind().String() + ": " + fmt.Sprintf("%g", value.Complex())
	case reflect.String:
		return value.Kind().String() + ": " + value.String()
	case reflect.Slice, reflect.Array:
		str := value.Kind().String() + ": "
		l := value.Len()
		strs := make([]string, l)
		for i := 0; i < l; i++ {
			strs[i] = fmt.Sprintf("%v", value.Index(i))
		}
		str += "[" + strings.Join(strs, ", ") + "]"
		return str
	case reflect.Map:
		str := "map: "
		strs := make([]string, 0)
		for _, k := range value.MapKeys() {
			strs = append(strs, fmt.Sprintf("%v: %v", k, value.MapIndex(k)))
		}
		str += "{" + strings.Join(strs, ", ") + "}"
		return str
	case reflect.Ptr:
		str := "ptr: "
		str += fmt.Sprintf("%v", value.Elem())
		return str
	case reflect.Struct:
		str := "struct: "
		l := value.NumField()
		strs := make([]string, l)
		for i := 0; i < l; i++ {
			strs[i] = fmt.Sprintf("%v", value.Field(i))
		}
		str += "{" + strings.Join(strs, ", ") + "}"
		return str

	default:
		return typ.Kind().String()
	}
	
	// return "";
}