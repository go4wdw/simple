package tests

import (
	"testing"
	"runtime"
	"fmt"
	"os"
	//"path/filepath"
)

func AssertEqual(t *testing.T, a interface{}, b interface{}) {
	if a != b {
		_, file, line, ok_ := runtime.Caller(1)
		if (ok_) {
			fmt.Fprintf(os.Stderr, "    %s:%d ", file, line)
		}
		fmt.Fprintf(os.Stderr, "%v != %v", a, b)
		fmt.Fprintln(os.Stderr)
		t.Fail()
	}
}
