package timeutils

import (
	"time"
	"sync"
)

type Sleeper interface {
	SleepMS(ms int64)
	Sleep(d time.Duration)
	// interrupt current sleep or next incoming sleep
	Interrupt()
}

type sleepImpl struct {
	timerChan  chan int
	sleepTimer *time.Timer
	mutex sync.Mutex
}

func NewSleeper() Sleeper {
	var sleeper sleepImpl
	sleeper.timerChan = make(chan int, 8)
	return &sleeper
}

func (thiz *sleepImpl) SleepMS(ms int64) {
	thiz.Sleep(time.Millisecond*time.Duration(ms))
}
func (thiz *sleepImpl) Sleep(d time.Duration) {
	if thiz.sleepTimer != nil {
		thiz.sleepTimer.Stop()
	}
	thiz.sleepTimer = time.AfterFunc(d, func() {
		thiz.timerChan <- 1
	})

	select {
	case <-thiz.timerChan:
	}
}
func (thiz *sleepImpl) Interrupt() {
	if thiz.sleepTimer != nil {
		thiz.sleepTimer.Stop()
	}

	// prevent block

	if len(thiz.timerChan) > 0 {
		return
	}

	thiz.mutex.Lock()
	defer thiz.mutex.Unlock()
	if len(thiz.timerChan) > 0 { // channel 如果已经有数据，就返回
		return
	}
	thiz.timerChan <- 1 // prevent block
}


// millisecond(ms 毫秒) of now
func NowMS() int64 {
	return time.Now().UnixNano() / 1e6
}

func IsSameDay(t1 *time.Time, t2 *time.Time) bool {
	if nil == t1 || nil == t2 {
		return false
	}
	return t1.Year() == t2.Year() && t1.Month() == t2.Month() && t1.Day() == t2.Day()
}