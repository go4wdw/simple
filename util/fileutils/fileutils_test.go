package fileutils

import (
	"log"
	"os"
	"testing"

	"wdw/simple/util/tests"
)

func TestCreateTempFile(t *testing.T) {
	arr := []string{}
	for i := 0 ; i < 100; i++ {
		dir, err := CreateTempFile("wdw_", "_abc", "x:\\test\\tmp")
		log.Printf("%s, %v", dir, err)
		arr = append(arr, dir)
	}

	for _, v := range arr {
		log.Printf("%s", v)
		//os.RemoveAll(v)
	}
}
func TestCreateTempDir(t *testing.T) {
	arr := []string{}
	for i := 0 ; i < 800; i++ {
		dir, err := CreateTempDir("wdw_", "_abc", "x:\\test\\tmp")
		log.Printf("%s, %v", dir, err)
		arr = append(arr, dir)
	}

	for _, v := range arr {
		os.RemoveAll(v)
	}
}


func TestIsEmptyDir(t *testing.T) {
	ok, err := IsEmptyDir("x:/dir")
	log.Printf("wdw: %v %v", ok ,err)
}

func TestIsSubDir(t *testing.T) {
	tests.AssertEqual(t, IsSubDir("", ""), true)
	tests.AssertEqual(t, IsSubDir("/usr", "/usr"), true)
	tests.AssertEqual(t, IsSubDir("/us", "/usr"), false)
	tests.AssertEqual(t, IsSubDir("/usr/", "/usr"), true)
	tests.AssertEqual(t, IsSubDir("/usra", "/usr"), false)
}