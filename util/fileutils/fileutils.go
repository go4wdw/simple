package fileutils

import (
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"

	"wdw/simple/util/randomutils"
	"wdw/simple/util/stringutils"
)

// create temp dir
//
const TEMP_FILE_CHARS = stringutils.LETTER_LOWER + stringutils.DIGIT

func CreateTempFile(prefix string, suffix string, dir string) (string, error) {
	if dir == "" {
		dir = os.TempDir()
	}

	var name string
	var err error

	// try create parent first
	err = os.MkdirAll(dir, 0700)
	if os.IsNotExist(err) {
		return "", err
	}


	create := func (tryTimes int, randLen int) (string, error) {
		for i := 0; i < tryTimes; i++ {
			try := filepath.Join(dir, prefix + randomutils.GetStr(TEMP_FILE_CHARS, randLen) + suffix)
			f, err := os.OpenFile(try, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
			if nil != f {
				f.Close()
			}
			if os.IsExist(err) {
				continue
			}
			if os.IsNotExist(err) {
				return "", err
			}
			return try, nil
		}
		return "", nil
	}



	name, err = create(1, 1)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(1, 2)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(1, 3)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 4)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 6)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 8)
	if "" != name || nil != err {
		return name, err
	}

	return "", errors.New("create failed: try too many times")

}


// create temp dir
// @return string created file path
// @return error 
func CreateTempDir(prefix string, suffix string, dir string) (string, error) {
	if dir == "" {
		dir = os.TempDir()
	}

	var name string
	var err error

	// try create parent first
	err = os.MkdirAll(dir, 0700)
	if os.IsNotExist(err) {
		return "", err
	}


	create := func (tryTimes int, randLen int) (string, error) {
		for i := 0; i < tryTimes; i++ {
			try := filepath.Join(dir, prefix + randomutils.GetStr(TEMP_FILE_CHARS, randLen) + suffix)
			err := os.Mkdir(try, 0700)
			if os.IsExist(err) {
				continue
			}
			if os.IsNotExist(err) {
				return "", err
			}
			return try, nil
		}
		return "", nil
	}



	name, err = create(1, 1)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(1, 2)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(1, 3)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 4)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 6)
	if "" != name || nil != err {
		return name, err
	}

	name, err = create(2, 8)
	if "" != name || nil != err {
		return name, err
	}

	return "", errors.New("create failed: try too many times")

}


// Copy the src file to dst. Any existing file will be overwritten and will not
// copy file attributes.
func Copy(src string, dst string) error {
    in, err := os.Open(src)
    if err != nil {
        return err
    }
    defer in.Close()

    out, err := os.Create(dst)
    if err != nil {
        return err
    }
    defer out.Close()

    _, err = io.Copy(out, in)
    if err != nil {
        return err
    }
    return out.Close()
}

func FileSize(file string) int64 {
	fi, err := os.Stat(file)
	if nil != err {
		return -1
	}
	return fi.Size()
}
func IsFile(file string) bool {
	fi, err := os.Stat(file)
	if nil != err {
		return false
	}
	if fi.IsDir() {
		return false
	}
	return true
}

func IsDir(file string) (bool, error) {
	fi, err := os.Stat(file)
	if nil != err {
		return false, err
	}
	return fi.IsDir(), nil
}

func IsEmptyDir(name string) (bool, error) {
    f, err := os.Open(name)
    if err != nil {
        return false, err
    }
    defer f.Close()

	//var isDir bool
	var fi os.FileInfo
	fi, err = f.Stat()
	if err != nil {
		return false, err
	}

	if !fi.IsDir() {
		return false, nil
	}


    _, err = f.Readdirnames(1) // or f.Readdir(1)
    if err == io.EOF {
        return true, nil
    }
    return false, err // Either not empty or error, suits both cases
}

func IsSubDir(child string , parent string) bool {


	child = strings.ReplaceAll(child, "\\", "/")
	child = filepath.Clean(child)
	parent = strings.ReplaceAll(parent, "\\", "/")
	parent = filepath.Clean(parent)

	lenP := len(parent)
	lenC := len(child)

	if lenC < lenP {
		return false
	}

	if child == parent {
		return true
	}


	if strings.HasPrefix(child, parent) && os.IsPathSeparator(child[lenP]) {
		return true
	}

	return false
}

func IsSymlink(mode os.FileMode) bool {
	return mode & os.ModeSymlink != 0
}