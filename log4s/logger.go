// logger system
// logger.GetLogger("name")
package log4s

import (
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"
	"path/filepath"

	"wdw/simple/util/stringutils"
	"wdw/simple/util/timeutils"
	"wdw/simple/util/fileutils"
)

func init() {
	fmt.Print("")
}

type Level int

const (
	DEBUG Level = iota
	INFO
	WARN
	ERROR
	FATAL
	NONE // disable log
)

const (
	sep = "|"
)


// a Logger must be created by GetLogger()
type Logger struct {
	fileName string // not contain date
	fileOpenTime time.Time
	file     *os.File
	level    Level
	isLineInfo bool
	withMS bool
}

type item struct {
	logger *Logger
	str *string
}

var writeChan chan item = make(chan item, 1024*8)

var loggerMap = make(map[string]*Logger)
var loggerMapMutex sync.Mutex
var logHomeDir string = "."
var logKeepDays int = -1 // 日志保留的天数

var delLogOnce sync.Once

func init() {
	go writeChanLog()
	//go cleanLog()
}

func writeChanLog() {
	for {
		select {
		case i := <-writeChan:
			i.logger.write0(i.str)		
		}
	}
}

func cleanLog() {
	cleanLog1()
	c := time.Tick(45 * time.Minute)
	for range c {
		cleanLog1()
	}
}

func cleanLog1() {
	days := logKeepDays
	if days <= 0 {
		return
	}
	for _, logger := range loggerMap {
		now := time.Now()
		now = now.AddDate(0, 0, days * -1)
		fileName := filepath.Join(logHomeDir, logger.fileName + "." + now.Format("2006-01-02"))
		if fileutils.IsFile(fileName) {
			os.Remove(fileName)
		}
	}
}

func Flush() {
	for {
		select {
		case i := <-writeChan:
			i.logger.write0(i.str)		
		default:
			return
		}
	}
}
func SetLogKeepDays(days int) {
	logKeepDays = days
	go delLogOnce.Do(cleanLog)
}
func SetLogHomeDir(dir string) {
	logHomeDir = dir
	err := os.MkdirAll(logHomeDir, 0700)
	if nil != err {
		fmt.Fprintf(os.Stderr, "mkdir log home dir err: %v\n", err)
	}
}

// get a logger
func GetLogger(name string) *Logger {
	logger, ok := loggerMap[name]
	if !ok {
		loggerMapMutex.Lock()
		if nil == logger {
			logger = addLogger(name)
		}
		loggerMapMutex.Unlock()
	}

	return logger
}

func (logger *Logger) SetLineInfo(isLineInfo bool) *Logger {
	logger.isLineInfo = isLineInfo
	return logger
}
func (logger *Logger) SetWithMS(withMS bool) *Logger {
	logger.withMS = withMS
	return logger
}

func (logger *Logger) Debug(a ...interface{}) {
	logger.log(DEBUG, a...)
}
func (logger *Logger) Info(a ...interface{}) {
	logger.log(INFO, a...)
}
func (logger *Logger) Warn(a ...interface{}) {
	logger.log(WARN, a...)
}
func (logger *Logger) Error(a ...interface{}) {
	logger.log(ERROR, a...)
}
func (logger *Logger) Fatal(a ...interface{}) {
	logger.log(FATAL, a...)
}

func (logger *Logger) Debugf(format string, a ...interface{}) {
	logger.logf(DEBUG, format, a...)
}
func (logger *Logger) Infof(format string, a ...interface{}) {
	logger.logf(INFO, format, a...)
}
func (logger *Logger) Warnf(format string, a ...interface{}) {
	logger.logf(WARN, format, a...)
}
func (logger *Logger) Errorf(format string, a ...interface{}) {
	logger.logf(ERROR, format, a...)
}
func (logger *Logger) Fatalf(format string, a ...interface{}) {
	logger.logf(FATAL, format, a...)
}

// close logger
func (logger *Logger) Close() {
	if nil != logger.file {
		logger.file.Close()
	}
}

func addLogger(name string) *Logger {
	logger := new(Logger)

	logger.isLineInfo = true
	logger.withMS = true

	logger.fileName = name


	loggerMap[name] = logger
	return logger
}

// create logger file
func (logger *Logger) openFile() {

	now := time.Now()

	fileName := filepath.Join(logHomeDir, logger.fileName + "." + now.Format("2006-01-02"))
	linkName := filepath.Join(logHomeDir, logger.fileName)

	// make sure is link
	r, err := os.Readlink(linkName)
	if r != "" && nil == err {
		os.Remove(linkName)
	}
	os.Symlink(filepath.Base(fileName), linkName)



	if nil != logger.file { // close old file first
		logger.file.Close()
	}

	//var err error
	logger.file, err = os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if nil != err {
		fmt.Fprintf(os.Stderr, "create log file err: %s, %v", fileName, err)
	}
	logger.fileOpenTime = now
}

// write log with sep
func (logger *Logger) log(lvl Level, a ...interface{}) {
	if lvl < logger.level {
		return
	}

	var str string

	if len(a) > 0 {
		val := fmt.Sprintf("%+v", a[0])
		str += sep + val
		for i := 1; i < len(a); i++ {
			val = fmt.Sprintf("%+v", a[i])
			str += sep + val
		}
	}

	logger.log0(lvl, &str)

}

func (logger *Logger) logf(lvl Level, format string, a ...interface{}) {
	if lvl < logger.level {
		return
	}

	var str string
	if len(format) > 0 {
		str += sep + fmt.Sprintf(format, a...)
	}

	logger.log0(lvl, &str)
}

func lvl2str(lvl Level) string {
	var str string
	switch lvl {
	case DEBUG:
		str = "D"
	case INFO:
		str = "I"
	case WARN:
		str = "W"
	case ERROR:
		str = "E"
	case FATAL:
		str = "F"
	}
	return str
}

func (logger *Logger) log0(lvl Level, str *string) {

	var line string
	var fmt string = "2006-01-02 15:04:05.000"
	if !logger.withMS {
		fmt = "2006-01-02 15:04:05"
	}
	line += time.Now().Format(fmt)

	line += sep + lvl2str(lvl)

	if logger.isLineInfo {
		file_, func_, lineno_ := stringutils.GetCaller(4)
		line += sep + file_ + "." + func_ + ":" + strconv.Itoa(lineno_)
	} else {
		line += sep
	}

	line += *str + stringutils.LineSeparator()

	//logger.write0(&line)
	i := item{logger, &line}
	writeChan <- i
}

// write to file
// if day change create a new file
func (logger *Logger) write0(str *string) {
	if nil == logger.file {
		logger.openFile()
	}

	now := time.Now()

	if now.Sub(logger.fileOpenTime) > time.Second * 60 {
		logger.openFile()
	}

	if !timeutils.IsSameDay(&now, &logger.fileOpenTime) {
		logger.openFile()
	}

	if nil == logger.file {
		fmt.Fprintf(os.Stderr, "log4s: %s\n", *str)
		return
	}
	logger.file.WriteString(*str)
}
