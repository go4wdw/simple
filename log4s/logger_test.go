package log4s_test

import (
	"fmt"
	"testing"
	"wdw/simple/log4s"
	//"time"
)

func TestT(t *testing.T) {
	/*
		var str string = nil
		fmt.Println(len(str))
	*/
}

func TestLog(t *testing.T) {
	t.Log("wdw")
	fmt.Println("wdw")
	log4s.SetLogHomeDir("log")
	log4s.SetLogKeepDays(3)
	log := log4s.GetLogger("test.log")
	log.Info()
	log.Info(1, nil, 3)
	log.Info("wdw", "abc", "def")
	log.Debugf("abc: %s, %d\n", "wdw", 3)
	log.Debugf("abc: %s, %d", "wdw", 3)
	log4s.Flush()
	//time.Sleep(time.Second * 3)
}
