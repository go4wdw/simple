/*

 http://man7.org/linux/man-pages/man5/proc.5.html
 /proc/[pid]/stat
              Status information about the process. 
              It is defined in the kernel source file fs/proc/array.c.


	stats := procstat.Stat{Pid: os.Getpid()}
	err := stats.Update()

 */
package procstat

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
)






type Stat struct {
	Pid int
	Comm string
	State byte
	PPid int
	PGrp int
	Session int
	TtyNr int
	Tpgid int
	Flags int64
	Minflt int64
	Cminflt uint64
	Majflt uint64
	Cmajflt uint64
	Utime uint64
	Stime uint64
	Cutime int64
	Cstime int64
	Priority int64
	Nice     int64
	NumThreads int64
	Itrealvalue int64
	Starttime uint64
	Vsize uint64
	Rss int64
	Rsslim uint64
	Startcode uint64
	Endcode uint64
	Startstack uint64
	Kstkesp uint64
	Kstkeip uint64
	Signal uint64
	Blocked uint64
	Sigignore uint64
	Sigcatch uint64
	Wchan uint64
	Nswap uint64
	Cnswap uint64
	ExitSignal int
	Processor int
	RtPriority uint
	Policy uint
	DelayacctBlkioTicks uint64
	GuestTime uint64
	CguestTime uint
}














func (s *Stat) Update() error {
	if s.Pid <= 0 {
		return errors.New("illegal pid")
	}

	path := filepath.Join("/proc", strconv.FormatInt(int64(s.Pid), 10), "stat")
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fscanf(file, "%d %s %c %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
		&s.Pid, 
		&s.Comm, 
		&s.State,
		&s.PPid,
		&s.PGrp,
		&s.Session,
		&s.TtyNr,
		&s.Tpgid,
		&s.Flags,
		&s.Minflt,
		&s.Cminflt,
		&s.Majflt,
		&s.Cmajflt,
		&s.Utime,
		&s.Stime,
		&s.Cutime,
		&s.Cstime,
		&s.Priority,
		&s.Nice,
		&s.NumThreads,
		&s.Itrealvalue,
		&s.Starttime,
		&s.Vsize,
		&s.Rss,
		&s.Rsslim,
		&s.Startcode,
		&s.Endcode,
		&s.Startstack,
		&s.Kstkesp,
		&s.Kstkeip,
		&s.Signal,
		&s.Blocked,
		&s.Sigignore,
		&s.Sigcatch,
		&s.Wchan,
		&s.Nswap,
		&s.Cnswap,
		&s.ExitSignal,
		&s.Processor,
		&s.RtPriority,
		&s.Policy,
		&s.DelayacctBlkioTicks,
		&s.GuestTime,
		&s.CguestTime)

	return err
}